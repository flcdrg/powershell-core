import-module au
. $PSScriptRoot\..\_scripts\all.ps1

$SemVerRegexWithCaptures = '(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?'

$VersionPrefixLatest = "7.1"
$VersionPrefixLTS = "7.0"
$VersionPrefixPreview = "7.2"

$releases    = 'https://github.com/PowerShell/PowerShell/releases'
$domain   = $releases -split '(?<=//.+)/' | select -First 1

function global:au_SearchReplace {
   @{
        "$($Latest.PackageName).nuspec" = @{
            "(\<dependency .+?`"powershell-core`" version=)`"([^`"]+)`"" = "`$1`"$($Latest.Version)`""
            "(\<releaseNotes\>).*?(\</releaseNotes\>)" = "`${1}$($Latest.ReleaseNotes)`$2"
        }
    }
}

#function global:au_BeforeUpdate { Get-RemoteFiles -Purge }
function global:au_AfterUpdate  { Set-DescriptionFromReadme -SkipFirst 2 }

function global:au_GetLatest {
    $download_page = Invoke-WebRequest -UseBasicParsing -Uri $releases

    #Latest
    $re  = "PowerShell-$VersionPrefixLatest.*-win-x64.msi"
    $url = $download_page.links | ? href -match $re | select -First 1 -expand href
    $url = "https://github.com" + $url
    $url32 = $url -replace 'x64.msi$', 'x86.msi'  
    $url -match $SemVerRegexWithCaptures
    $version = $matches[0]

    #LTS
    $re_LTS  = "PowerShell-$VersionPrefixLTS.*-win-x64.msi"
    $url_LTS = $download_page.links | ? href -match $re_LTS | select -First 1 -expand href
    $url_LTS = "https://github.com" + $url_LTS
    $url32_LTS = $url_LTS -replace 'x64.msi$', 'x86.msi'  
    $url_LTS -match $SemVerRegexWithCaptures
    $version_LTS = $matches[0]

    #Preview
    $re_Preview  = "*PowerShell-$VersionPrefixPreview*review.*-win-x64.msi*"
    $url_Preview = $download_page.links | ? href -ilike $re_Preview | select -First 1 -expand href
    $url_Preview = "https://github.com" + $url_Preview
    $url32_Preview = $url_Preview -replace 'x64.msi$', 'x86.msi'  
    $url_Preview -match $SemVerRegexWithCaptures
    $version_Preview = $matches[0]

    return @{
      Streams = [ordered] @{
	    	'Latest' = @{        
          URL64        = $url;
          URL32        = $url32;
          Version      = $version;
          ReleaseNotes = "$releases/tag/${version}";
          PackageName = 'pwsh';
        }
	    	'LTS' = @{        
          URL64        = $url_LTS;
          URL32        = $url32_LTS;
          Version      = $version_LTS;
          ReleaseNotes = "$releases/tag/${version_LTS}";
          PackageName = 'pwsh';
        } 
	    	'Preview' = @{        
          URL64        = $url_Preview;
          URL32        = $url32_Preview;
          Version      = $version_Preview -replace "-preview.", "-preview" -replace ' ', '';
          ReleaseNotes = "$releases/tag/${version_Preview}";
          PackageName = 'pwsh';
        }            
    }
  }
}

update -ChecksumFor none
